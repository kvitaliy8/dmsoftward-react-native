'use strict'

import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Picker,
  Modal,
  TouchableOpacity,
  TouchableNativeFeedback,
  Text,
  Button,
  PixelRatio
} from 'react-native';
import moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';

export default class DatePickerView extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isDateTimePickerVisible: false
    }
  }

  _handleDatePicked = (date) => {
    this.props.onConfirm(date.toString())
    this._hideDateTimePicker();
  };

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  render() {
    return (
      <TouchableOpacity style={this.props.style}
        onPress={this._showDateTimePicker.bind(this)}>
        <View>
          <Text style={this.props.textStyle}>
            {this.props.value ? moment(Date.parse(this.props.value)).format(this.props.format) : this.props.placeholder ? <Text style={[{color: 'gray'}, this.props.placeholderStyle]}>{this.props.placeholder}</Text> : ''}
          </Text>
          <DateTimePicker
            mode={this.props.mode}
            date={this.props.value ? new Date(this.props.value) : new Date()}
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this._handleDatePicked}
            onCancel={this._hideDateTimePicker}/>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  spaceContainer: {
    flex: 1
  },
  pickerContainer: {
    backgroundColor: '#FFFFFF',
    justifyContent: 'center'
  },
  buttonBar: {
    flexDirection: 'row',
    backgroundColor: '#FFFFFF',
    borderBottomWidth: 1 / PixelRatio.get(),
    borderBottomColor: 'grey'
  },
  buttonBarSpace: {
    flex: 1
  }
});
