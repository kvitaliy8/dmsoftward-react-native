import {
  Image,
  StyleSheet,
  View,
  Text,
  ScrollView,
  TextInput,
  Picker,
  Platform,
  Alert
} from 'react-native'
import React, { Component } from 'react';
import {
  List,
  ListItem,
  Grid,
  Col,
  Row,
  FormLabel, Button, FormInput,Icon
} from 'react-native-elements'
var SignaturePad = require('react-native-signature-pad');
import PickerView from '../../lib/picker';
import DatePickerView from '../common/DatePicker';
import * as Storage from '../../lib/localstorage';
import colors from 'HSColors';
import Api from '../../lib/api';
import _ from 'lodash';
import Spinner from '../../lib/spinner';

var styles = require('../../styles/styles');
import { Actions } from 'react-native-router-flux';
const Item = Picker.Item;

class Break extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      shift_startdate_time: "",
      shift_enddate_time: ""
    };

    Storage.get('userId').then((value) => {
      this.setState({
        userId: value
      });
    });

    Storage.get('onbreak').then((value) => {
      this.setState({
        onbreak: value
      });
    });
  }

  _submitTime = () => {
    if(!this.state.shift_startdate_time || !this.state.shift_enddate_time){
      Alert.alert('Error', 'Please select time');
      return;
    }

    this.setState({
      loading: true
    })

    let that = this;
    let st = new Date(this.state.shift_startdate_time)
    let et = new Date(this.state.shift_enddate_time)
    let shift_time = {
      shift_startdate_time: st.toISOString(),
      shift_enddate_time: et.toISOString(),
      driverid: this.state.userId
    }

    console.log(shift_time)

    Api.post('savedrivershift', shift_time).then((data) => {
      this.setState({
        loading: false,
        shift_startdate_time: "",
        shift_enddate_time: ""
      })

      setTimeout(() => Alert.alert('Success', 'Time Logged'), 100)
    }).catch((err) => {
      console.log(err);
      this.setState({
        loading: false
      })
    });
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <ScrollView style={{ paddingTop: 20, flex: 1, backgroundColor: colors.grey6 }}>
          <View style={{ paddingTop: 0, marginTop:15, marginHorizontal:7}}>
            <View style={[styles.singleLineBox,{paddingHorizontal : 10}]}>
              <Grid>
                <Row>
                  <Col style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={[styles.labelText,{ fontSize: 23}]}>Shift Timing</Text>
                  </Col>
                </Row>
              </Grid>
            </View>
            <View style={[styles.singleLineBox,{paddingHorizontal : 10, marginVertical:3,paddingVertical : 10}]}>
              <Grid>
                <Row>
                  <Col style={{ flex: 1, justifyContent: 'center', alignItems: 'center',paddingVertical:5}}>
                    <Text style={[styles.labelText,{color:colors.grey8}]}>Select Start and End time</Text>
                  </Col>
                </Row>
                <Row>
                  <Col style={{ flex: 1, justifyContent: 'center', alignItems: 'center',paddingVertical:5}}>
                    <DatePickerView
                      mode={"time"}
                      style={{
                        width: 220,
                        height: 50,
                        backgroundColor:'#FFF',
                        justifyContent: 'center'
                      }}
                      placeholder="Start Time"
                      textStyle={[styles.labelText, {
                        color: colors.grey8,
                        paddingLeft: 15
                      }]}
                      format={'HH:mm'}
                      value={this.state.shift_startdate_time}
                      onConfirm={(shift_startdate_time) => this.setState({shift_startdate_time})} />
                  </Col>
                </Row>
                <Row>
                  <Col style={{ flex: 1, justifyContent: 'center', alignItems: 'center',paddingVertical:5}}>
                    <DatePickerView
                      mode={"time"}
                      style={{
                        width: 220,
                        height: 50,
                        backgroundColor:'#FFF',
                        justifyContent: 'center'
                      }}
                      placeholder="End Time"
                      textStyle={[styles.labelText, {
                        color: colors.grey8,
                        paddingLeft: 15
                      }]}
                      format={'HH:mm'}
                      value={this.state.shift_enddate_time}
                      onConfirm={(shift_enddate_time) => this.setState({shift_enddate_time})} />
                  </Col>
                </Row>
              </Grid>
            </View>
            <View style={[styles.singleLineBox,{paddingHorizontal : 0, minHeight:70, marginVertical:2,paddingVertical : 10}]}>
              <Grid>
                <Row>
                  <Col>
                    <Button borderRadius={5} buttonStyle={{paddingHorizontal : 25,paddingVertical : 9}} title="CANCEL" onPress={() => Actions.pop({refresh : {}})}></Button>
                  </Col>
                  <Col>
                    <Button borderRadius={5} buttonStyle={{paddingHorizontal : 25,paddingVertical : 9}} backgroundColor={colors.stumbleupon} title="SUBMIT" onPress={() => this._submitTime()}></Button>
                  </Col>
                </Row>
              </Grid>
            </View>
          </View>
        </ScrollView>
        <Spinner visible={this.state.loading} />
      </View>
    )
  }
}

module.exports = Break;
